/*
 * LICENSE: See the project's top-level LICENSE file.
 */
package org.lekic.dide;


import com.formdev.flatlaf.FlatDarkLaf;
import javax.swing.*;
import java.awt.BorderLayout;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;
import org.fife.ui.rtextarea.*;
import org.fife.ui.rsyntaxtextarea.*;

public class Main extends JFrame {

   public Main() {
       FlatDarkLaf.install();
       
      JPanel cp = new JPanel(new BorderLayout());
      
      RSyntaxTextArea textArea = new RSyntaxTextArea(20, 60);
      textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
      textArea.setCodeFoldingEnabled(true);
      RTextScrollPane sp = new RTextScrollPane(textArea);
      
      // RSTA theme
      var themePath = "/org/fife/ui/rsyntaxtextarea/themes/dark.xml";
       try {
           var theme = Theme.load(getClass().getResourceAsStream(themePath));
           theme.apply(textArea);
       } catch (IOException ex) {
           Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
       }
      
      cp.add(sp);

      setContentPane(cp);
      setTitle("DIDE: default");
      setDefaultCloseOperation(EXIT_ON_CLOSE);
      pack();
      setLocationRelativeTo(null);

   }

   public static void main(String[] args) {
       var appConfig = new AppConfig();
      // Start all Swing applications on the EDT.
      SwingUtilities.invokeLater(() -> {
        new Main().setVisible(true);
      });
   }

}