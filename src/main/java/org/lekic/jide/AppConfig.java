/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lekic.dide;

import java.io.*;
import com.google.gson.JsonParser;
import com.google.gson.JsonObject;

/**
 *
 * @author bmll
 */
public class AppConfig {
    // Yaml yaml;
    
    public AppConfig() {
        try {
            FileReader fileReader = new FileReader("/home/bmll/.config/dide/dide.json");
            JsonObject jsonObject = JsonParser.parseReader(fileReader).getAsJsonObject();
            
            String foo = jsonObject.get("foo").getAsString();
            System.out.println(foo);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

//        JsonReader rdr;
//          JsonObject jsonObject = new JsonParser().
//        LoadSettings settings = LoadSettings.builder().setLabel("Custom user configuration").build();
//        Load load = new Load(settings);
//        try {
//            var inputStream = new FileInputStream("/home/bmll/.config/dide/dide.yml");
//            // Iterable<Object> v = load.loadAllFromInputStream(inputStream);
//            Iterable<Object> v = load.loadAllFromReader(new StringReader("{foo: bar, list: [1, 2, 3]}"));
//            Iterator<Object> iter = v.iterator();
//            Object o1 = iter.next();
//            System.out.println(o1);
//        } catch (FileNotFoundException ex) {
//            ex.printStackTrace();
//        }
    }
}
